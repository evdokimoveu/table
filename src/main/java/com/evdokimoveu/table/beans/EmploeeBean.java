
package com.evdokimoveu.table.beans;

import com.evdokimoveu.table.entity.Emploee;
import com.evdokimoveu.table.entity.ProfessionName;
import com.evdokimoveu.table.services.EmploeeServices;
import com.evdokimoveu.table.services.ProfessionNameServices;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import org.apache.log4j.Logger;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.CellEditEvent;

/**
 *
 * @author Evdokymov Evgeny
 */

@ManagedBean(name="emploeeBean")
@RequestScoped
public class EmploeeBean implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private static final Logger log = Logger.getLogger(EmploeeBean.class);
    
    private List<Emploee> emploees;
    private Map<Long, String> emploeeProfName;
    
    /*Emploee fields*/
    private long id;
    private String name;
    private String surname;
    private String patronomic;
    private int salary;
    private int leaveCount;
    private String sex;
    private String tabelnum;
    private ProfessionName professionName;
    private long profNameId;
    
    /*Services*/
    @ManagedProperty(value="#{EmploeeServicesImpl}")
    private EmploeeServices emploeeServise;
    
    @ManagedProperty(value="#{ProfessionNameServicesImpl}")
    private ProfessionNameServices professionNameServices;

    /**
     * Remove selected Emploee from database
     * @param Emploee
     * @return 
     */
    public String removeEmploee(Emploee emploee){        
        emploeeServise.removeEmploee(emploee.getId());
        emploees.remove(emploee);
        getEmploees();
        return null;
    }
    /**
     * Event for edit table
     * @param CellEditEvent event
     * @return ""
     */
    public String onCellEdit(CellEditEvent event){
        Object oldValue = event.getOldValue();
        Object newValue = event.getNewValue();
        if(newValue != null && !newValue.equals(oldValue)) {
            DataTable table = (DataTable) event.getSource();
            Emploee emploee = (Emploee) table.getRowData();
            if(profNameId != 0){
                emploee.setProfName(professionNameServices.getProfessoinNameById(profNameId));
            }
            emploeeServise.updateEmploee(emploee);            
        }
        return null;
    }    
    
   /**
    * Insert new Emploee into the database
    * @return 
    */
    public String addNewEmploee(){
        Emploee newEmploee = createNewEmploee();
        emploeeServise.addNewEmploee(newEmploee);
        clear();        
        return null;
    }
    
     /**
     * Create new entity Emploee
     * @return Emploee
     */
    private Emploee createNewEmploee(){
        Emploee newEmploee = new Emploee();
        newEmploee.setName(this.getName());
        newEmploee.setSurname(this.getSurname());
        newEmploee.setPatronomic(this.getPatronomic());
        newEmploee.setSalary(this.getSalary());
        newEmploee.setSex(this.getSex());
        newEmploee.setTabelnum(this.getTabelnum());
        newEmploee.setLeaveCount(0);
        newEmploee.setProfName(professionNameServices.getProfessoinNameById(profNameId));
        return newEmploee;
    }
    
    /**
     * reset form id="formAddEmploee"
     * and bean variable
     */
    private void clear() {
        this.setName("");
        this.setSurname("");
        this.setPatronomic("");
        this.setSalary(0);        
        this.setSex("");
        this.setTabelnum("");
        this.setLeaveCount(0);
        this.setProfessionName(null);
    }    
    
    /**
     * Getters and Setters
     */
    
    /**
     *  @return List of Emploee from database
     */
    public List<Emploee> getEmploees() {
        if(emploees == null){
            emploees = new ArrayList<Emploee>();
            emploees.addAll(getEmploeeServise().getAllEmploee());            
        }
        return emploees;
    }
    
    public void setEmploees(List<Emploee> emploees) {
        this.emploees = emploees;
    }
    
    public EmploeeServices getEmploeeServise() {
        return emploeeServise;
    }

    public void setEmploeeServise(EmploeeServices emploeeServise) {
        this.emploeeServise = emploeeServise;
    }
    
    public ProfessionNameServices getProfessionNameServices() {
        return professionNameServices;
    }

    public void setProfessionNameServices(ProfessionNameServices professionNameServices) {
        this.professionNameServices = professionNameServices;
    }

    public ProfessionName getProfessionName() {
        return professionName;
    }

    public void setProfessionName(ProfessionName professionName) {
        this.professionName = professionName;
    }

    public long getId() {
        return id;
    }
   
    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
   
    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }    

    public String getPatronomic() {
        return patronomic;
    }

    public void setPatronomic(String patronomic) {
        this.patronomic = patronomic;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public int getLeaveCount() {
        return leaveCount;
    }
    
    public void setLeaveCount(int leaveCount) {
        this.leaveCount = leaveCount;
    }

    /**
     * Create Map of ProfName for SelectOneMenu
     * @return Map<Long, String>
     */    
    public Map<Long, String> getEmploeeProfName() {
        if(emploeeProfName == null){
            emploeeProfName = professionNameServices.getAllProfessionNameLikeMap();
        }
        return emploeeProfName;  
    }

    public void setEmploeeProfName(Map<Long, String> emploeeProfName) {
        this.emploeeProfName = emploeeProfName;
    }

    public long getProfNameId() {
        return profNameId;
    }

    public void setProfNameId(long profNameId) {
        this.profNameId = profNameId;
    }
    
    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getTabelnum() {
        return tabelnum;
    }

    public void setTabelnum(String tabelnum) {
        this.tabelnum = tabelnum;
    }  

}
