
package com.evdokimoveu.table.beans;

import com.evdokimoveu.table.entity.ProfessionCode;
import com.evdokimoveu.table.services.ProfessionCodeServices;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import org.apache.log4j.Logger;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.CellEditEvent;

/**
 *
 * @author Evdokymov Evgeny
 */
@ManagedBean(name="profCodeBean")
@RequestScoped
public class ProfCodeBean implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private static final Logger log = Logger.getLogger(ProfCodeBean.class);
    
    private List<ProfessionCode> profCodes;
        
    @ManagedProperty(value="#{ProfessionCodeServicesImpl}")
    private ProfessionCodeServices professionCodeServices;
    
    /*ProfessionName fields*/
    private long id;
    private String name;
    
    public String removeProfCode(ProfessionCode profCode){
        professionCodeServices.removeProfCode(profCode.getId());
        profCodes.remove(profCode);
        getProfCodes();
        return null;
    }
    /**
     * Event for edit table
     * @param CellEditEvent event
     * @return ""
     */
    public String onCellEdit(CellEditEvent event){        
        Object oldValue = event.getOldValue();
        Object newValue = event.getNewValue();
        
        if(newValue != null && !newValue.equals(oldValue)) {            
            DataTable table = (DataTable) event.getSource();
            ProfessionCode profCode = (ProfessionCode) table.getRowData();
            professionCodeServices.updateProfCode(profCode);
        }
        return null;
    }
    
    public String addNewProfCode(){
        ProfessionCode newProfCode = createNewProfCode();
        professionCodeServices.addNewProfCode(newProfCode);
        clear();
        return null;
    }

    /**
     * Create List of ProfCode for SelectOneMenu
     * @return List<ProfessionCode>
     */   
    public List<ProfessionCode> getProfCodes() {
        if(profCodes == null){
            profCodes = new ArrayList<ProfessionCode>();
            profCodes.addAll(getProfessionCodeServices().getAllProfessoinCode());
        }
        return profCodes;
    }

    public void setProfCodes(List<ProfessionCode> profCodes) {
        this.profCodes = profCodes;
    }

    public ProfessionCodeServices getProfessionCodeServices() {
        return professionCodeServices;
    }

    public void setProfessionCodeServices(ProfessionCodeServices professionCodeServices) {
        this.professionCodeServices = professionCodeServices;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private ProfessionCode createNewProfCode() {
        ProfessionCode newProfCode = new ProfessionCode();
        newProfCode.setName(this.getName());        
        return newProfCode;
    }

    private void clear() {
        this.setName("");
    }
    
}
