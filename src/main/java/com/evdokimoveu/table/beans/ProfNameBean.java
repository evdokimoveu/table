
package com.evdokimoveu.table.beans;

import com.evdokimoveu.table.entity.ProfessionName;
import com.evdokimoveu.table.services.ProfessionCodeServices;
import com.evdokimoveu.table.services.ProfessionNameServices;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import org.apache.log4j.Logger;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.CellEditEvent;

/**
 *
 * @author Evdokymov Evgeny
 */

@ManagedBean(name="profNameBean")
@RequestScoped
public class ProfNameBean implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private static final Logger log = Logger.getLogger(ProfNameBean.class);
    
    private List<ProfessionName> profNames;
    private Map<Long, String> profCodes;
    
    @ManagedProperty(value="#{ProfessionNameServicesImpl}")
    private ProfessionNameServices professionNameServices;
    
    @ManagedProperty(value="#{ProfessionCodeServicesImpl}")
    private ProfessionCodeServices professionCodeServices;
    
    /*ProfessionName fields*/
    private long id;
    private String name;
    private long profCodeId;
    
    /**
     * Delete ProfName from database
     * @param profName
     * @return 
     */
    public String removeProfName(ProfessionName profName){
        professionNameServices.removeProfName(profName.getId());
        profNames.remove(profName);
        getProfNames();
        return null;
    } 
       
    /**
     * Event for edit table
     * @param CellEditEvent event
     * @return ""
     */
    public String onCellEdit(CellEditEvent event){        
        Object oldValue = event.getOldValue();
        Object newValue = event.getNewValue();
        
        if(newValue != null && !newValue.equals(oldValue)) {            
            DataTable table = (DataTable) event.getSource();
            ProfessionName profName = (ProfessionName) table.getRowData();
            if(profCodeId != 0){
                profName.setProfCode(professionCodeServices.getProfessoinCodeById(profCodeId));
            }
            professionNameServices.updateProfName(profName);
        }
        return null;
    }
    
    
    public String addNewProfName(){
        ProfessionName newProfName = createNewProfName();
        professionNameServices.addNewProfName(newProfName);
        clear();        
        return null;
    }
            
    /**
     *  @return List of ProfessionName
     */
    public List<ProfessionName> getProfNames() {
        if(profNames == null){
            profNames = new ArrayList<ProfessionName>();
            profNames.addAll(getProfessionNameServices().getAllProfessoinName());
        }
        return profNames;
    }

    public void setProfNames(List<ProfessionName> profNames) {
        this.profNames = profNames;
    }

    public Map<Long, String> getProfCodes() {
        if(profCodes == null){
            profCodes = professionCodeServices.getAllProfessionCodeLikeMap();
        }
        return profCodes;        
    }

    public void setProfCodes(Map<Long, String> profCodes) {
        this.profCodes = profCodes;
    }

    public ProfessionNameServices getProfessionNameServices() {
        return professionNameServices;
    }

    public void setProfessionNameServices(ProfessionNameServices professionNameServices) {
        this.professionNameServices = professionNameServices;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getProfCodeId() {
        return profCodeId;
    }

    public void setProfCodeId(long profCodeId) {
        this.profCodeId = profCodeId;
    }

    public ProfessionCodeServices getProfessionCodeServices() {
        return professionCodeServices;
    }

    public void setProfessionCodeServices(ProfessionCodeServices professionCodeServices) {
        this.professionCodeServices = professionCodeServices;
    }
    
    private ProfessionName createNewProfName() {
        ProfessionName newProfName = new ProfessionName();
        newProfName.setName(this.getName());
        newProfName.setProfCode(professionCodeServices.getProfessoinCodeById(profCodeId));
        return newProfName;
    }

    private void clear() {
        this.setName("");
        this.setProfCodeId(0);
    }    
}
