
package com.evdokimoveu.table.beans;

import com.evdokimoveu.table.entity.StatDay;
import com.evdokimoveu.table.services.StatDayServices;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import org.apache.log4j.Logger;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.CellEditEvent;

/**
 *
 * @author Evdokymov Evgeny
 */

@ManagedBean(name="statDayBean")
@RequestScoped
public class StatDayBean implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private static final Logger log = Logger.getLogger(StatDayBean.class);
    
    private List<StatDay> statDays;
        
    @ManagedProperty(value="#{StatDayServicesImpl}")
    private StatDayServices statDayServices;
    
    /*StatDay fields*/
    private long id;
    private String name;
    private String description;


    public String removeStatDay(StatDay statDay){
        statDayServices.removeStatDay(statDay.getId());
        statDays.remove(statDay);
        getStatDays();
        return null;
    }
    
        
    /**
     * Event for edit table
     * @param CellEditEvent event
     * @return ""
     */
    public String onCellEdit(CellEditEvent event){        
        Object oldValue = event.getOldValue();
        Object newValue = event.getNewValue();
        
        if(newValue != null && !newValue.equals(oldValue)) {            
            DataTable table = (DataTable) event.getSource();
            StatDay statDay = (StatDay) table.getRowData();
            statDayServices.updateStatDay(statDay);
        }
        return null;
    }
    
    /**
     * Add new StatDay to database
     * @return 
     */ 
    public String addNewStatDay(){
        StatDay newStatDay = createNewStatDay();
        statDayServices.addNewStatDay(newStatDay);
        clear();
        return null;
    }
     
    /**
     *  @return List of Emploee 
     */
    public List<StatDay> getStatDays() {
        if(statDays == null){
            statDays = new ArrayList<StatDay>();
            statDays.addAll(getStatDayServices().getAllStatDay());
        }
        return statDays;
    }

    public void setStatDays(List<StatDay> statDays) {
        this.statDays = statDays;
    }    


    public StatDayServices getStatDayServices() {
        return statDayServices;
    }

    public void setStatDayServices(StatDayServices statDayServices) {
        this.statDayServices = statDayServices;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }
    

    public void setName(String name) {
        this.name = name;
    }    

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
           
    /**
     * Create new entity StatDay from bean variable
     * @return StatDay
     */
    private StatDay createNewStatDay() {
        StatDay newStatDay = new StatDay();
        newStatDay.setName(this.getName());
        newStatDay.setDescription(this.getDescription());
        return newStatDay;
    }
    
       
    /**
     * reset form jsf table
     * and bean variable
     */
    private void clear() {
        this.setName("");
        this.setDescription("");
    }
        
}
