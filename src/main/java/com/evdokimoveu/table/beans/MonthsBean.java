
package com.evdokimoveu.table.beans;

import com.evdokimoveu.table.entity.Months;
import com.evdokimoveu.table.services.MonthsServices;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import org.apache.log4j.Logger;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.CellEditEvent;

/**
 *
 * @author Evdokymov Evgeny
 */

@ManagedBean(name="monthsBean")
@RequestScoped
public class MonthsBean implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private static final Logger log = Logger.getLogger(MonthsBean.class);
        
    private List<Months> months;
    
    @ManagedProperty(value="#{MonthsServicesImpl}")
    private MonthsServices monthsServices;
    
        
    /*Tabel fields*/    
    private long monthId;
    private String name;
    
        
    /**
     * Event for edit table
     * @param CellEditEvent event
     * @return ""
     */
    public String onCellEdit(CellEditEvent event){        
        Object oldValue = event.getOldValue();
        Object newValue = event.getNewValue();
        
        if(newValue != null && !newValue.equals(oldValue)) {            
            DataTable table = (DataTable) event.getSource();
            Months month = (Months) table.getRowData();
            monthsServices.updateMonth(month);            
        }
        return null;
    }
    
    public List<Months> getMonths() {
        if(months == null){
            months = new ArrayList<Months>();
            months.addAll(getMonthsServices().getAllMonths());
        }
        return months;
    }

    public void setMonths(List months) {
        this.months = months;
    }

    public MonthsServices getMonthsServices() {
        return monthsServices;
    }

    public void setMonthsServices(MonthsServices monthsServices) {
        this.monthsServices = monthsServices;
    }

    public long getMonthId() {
        return monthId;
    }

    public void setMonthId(long monthId) {
        this.monthId = monthId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
}
