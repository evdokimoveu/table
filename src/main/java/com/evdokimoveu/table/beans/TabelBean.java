
package com.evdokimoveu.table.beans;

import com.evdokimoveu.table.entity.StatDay;
import com.evdokimoveu.table.entity.WorkDay;
import com.evdokimoveu.table.services.EmploeeServices;
import com.evdokimoveu.table.services.MonthsServices;
import com.evdokimoveu.table.services.StatDayServices;
import com.evdokimoveu.table.services.TabelServices;
import com.evdokimoveu.table.services.WorkDayServices;
import com.evdokimoveu.table.services.YearsServices;
import com.evdokimoveu.table.util.CreateClearWorkDays;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.apache.log4j.Logger;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.CellEditEvent;

/**
 *
 * @author Evdokymov Evgeny
 */

@ManagedBean(name="tabelBean")
@ViewScoped
public class TabelBean implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private static final Logger log = Logger.getLogger(TabelBean.class);
    
    private Map<Long, String> emploees;
    private Map<Long, String> statDays;
    private Map<Long, String> years;
    private Map<Long, String> months;
    private List<WorkDay> tabels;    

    private int year;
    private int month;
        
    /*Tabel fields Id*/
    private long emploeeId;
    private long monthId;
    private long yearId;
    private long statDayId;
    private Long tabelId;
    
    /*Services*/    
    @ManagedProperty(value="#{EmploeeServicesImpl}")
    private EmploeeServices emploeeServise;
    
    @ManagedProperty(value="#{StatDayServicesImpl}")
    private StatDayServices statDayServices;
    
    @ManagedProperty(value="#{YearsServicesImpl}")
    private YearsServices yearsServices;
    
    @ManagedProperty(value="#{MonthsServicesImpl}")
    private MonthsServices monthsServices;
            
    @ManagedProperty(value="#{TabelServicesImpl}")
    private TabelServices tabelServices;
    
    @ManagedProperty(value="#{WorkDayServicesImpl}")
    private WorkDayServices workDayServices;

    /**
     * Cheack and add current year and months table if it not exists in database
     */    
    @PostConstruct
    public void init(){
        yearsServices.addNewYear();
        monthsServices.createNewMonthsTable();
    }
    
    /**
     * Load table from database for tabel.xhtml
     * If data is not exists in database, use CreateClearWorkDays to create new data 
     * @return ""
     */
    public String loadTabel(){
        year = new Integer(yearsServices.getYearById(yearId).getName());
        month = monthsServices.getMonthById(monthId).getId_months().intValue();
        tabelId = tabelServices.getTabelId(emploeeId, year, month);        
        if(tabelId != null){
            tabels = workDayServices.getWorkDays(tabelId);
        }
        else{
            StatDay statDay = statDayServices.getStatDayById(1);
            if(statDay == null){
                statDay = new StatDay();
                statDay.setId(1);
                statDay.setName("WD");
                statDay.setDescription("Work Day");                
                statDayServices.addNewStatDay(statDay);
            }
            tabels = CreateClearWorkDays.getNewWorkDays(month-1, year, statDay);            
        }        
        return null;
    }
            
    /**
     * Event for edit table
     * @param CellEditEvent event
     * @return ""
     */
    public String onCellEdit(CellEditEvent event){
        Object oldValue = event.getOldValue();
        Object newValue = event.getNewValue();
        if(newValue != null && !newValue.equals(oldValue)) {
            DataTable table = (DataTable) event.getSource();
            WorkDay workDay = (WorkDay) table.getRowData();
            if(statDayId != 0){
                workDay.setStatDay(statDayServices.getStatDayById(statDayId));
            }
            tabels.set(workDay.getDayOfMonth()-1, workDay);
        }
        return null;
    }
    
    /**
     * Save table to database after users edit
     * @return 
     */
    public String saveTabel(){
        if(tabels != null && !tabels.isEmpty()){
            workDayServices.addWorkDayByPackage(emploeeId, tabels, year, month);
            tabels.clear();
        }
        return null;
    }
    
    /**
     *                Getters and Setters   
     */
    
    public Map<Long, String> getEmploees() {
        if(emploees == null){
            emploees = emploeeServise.getAllEmploeeLikeMap();
        }
        return emploees;
    }
    
    public long getEmploeeId() {
        return emploeeId;
    }

    public void setEmploeeId(long emploeeId) {
        this.emploeeId = emploeeId;
    }

    
    public void setEmploees(Map<Long, String> emploees) {
        this.emploees = emploees;
    }
    
    public Map<Long, String> getStatDays() {
       if(statDays == null){
            statDays = statDayServices.getAllStatDayLikeMap();
        }
        return statDays;     
    }

    public void setStatDays(Map<Long, String> statDays) {
        this.statDays = statDays;
    }
    
    public EmploeeServices getEmploeeServise() {
        return emploeeServise;
    }

    public void setEmploeeServise(EmploeeServices emploeeServise) {
        this.emploeeServise = emploeeServise;
    }

    public StatDayServices getStatDayServices() {
        return statDayServices;
    }

    public void setStatDayServices(StatDayServices statDayServices) {
        this.statDayServices = statDayServices;
    }

    public Map<Long, String> getYears() {
        if(years == null){
            years = yearsServices.getAllYearsLikeMap();
        }
        return years;
    }

    public void setYears(Map<Long, String> years) {
        this.years = years;
    }

    public Map<Long, String> getMonths() {
        if(months == null){
            months = monthsServices.getAllMonthsLikeMap();
        }
        return months;
    }

    public void setMonths(Map<Long, String> months) {
        this.months = months;
    }

    public long getMonthId() {
        return monthId;
    }

    public void setMonthId(long monthId) {
        this.monthId = monthId;
    }

    public long getYearId() {
        return yearId;
    }

    public void setYearId(long yearId) {
        this.yearId = yearId;
    }

    public YearsServices getYearsServices() {
        return yearsServices;
    }

    public void setYearsServices(YearsServices yearsServices) {
        this.yearsServices = yearsServices;
    }

    public MonthsServices getMonthsServices() {
        return monthsServices;
    }

    public void setMonthsServices(MonthsServices monthsServices) {
        this.monthsServices = monthsServices;
    }

    public List<WorkDay> getTabels() {
        return tabels;
    }

    public void setTabels(List<WorkDay> tabels) {
        this.tabels = tabels;
    }    
    
    public TabelServices getTabelServices() {
        return tabelServices;
    }

    public void setTabelServices(TabelServices tabelServices) {
        this.tabelServices = tabelServices;
    }

    public long getStatDayId() {
        return statDayId;
    }

    public void setStatDayId(long statDayId) {
        this.statDayId = statDayId;
    }

    public WorkDayServices getWorkDayServices() {
        return workDayServices;
    }

    public void setWorkDayServices(WorkDayServices workDayServices) {
        this.workDayServices = workDayServices;
    }

}
