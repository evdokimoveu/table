
package com.evdokimoveu.table.beans;

import java.io.IOException;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import org.apache.log4j.Logger;

/**
 * 
 * @author Evdokymov Evgeny
 */
@ManagedBean(name="loginBean")
@RequestScoped
public class LoginBean implements Serializable{
    
    private static final long serialVersionUID = 1L;
    
    private static final Logger log = Logger.getLogger(LoginBean.class);
    
    private String userName = null;
    private String password = null;


    /** 
     * Check login and password of user
     * @return String
     * @throws ServletException
     * @throws IOException 
     */
    public String doLogin() throws IOException, ServletException{
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        RequestDispatcher dispatcher = ((ServletRequest) context.getRequest()).getRequestDispatcher("/j_spring_security_check");
        dispatcher.forward((ServletRequest)context.getRequest(), (ServletResponse)context.getResponse());
        FacesContext.getCurrentInstance().responseComplete();
        return null;
    }
    
    /**
     * User logout
     * @return String
     * @throws IOException 
     */
    public String logout() throws IOException{
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        context.invalidateSession();
        context.redirect(context.getRequestContextPath() + "/login.xhtml");
        return null;
    }
    /**
     * Get login
     * @return String userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Set login 
     * @param String userName 
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * Get password
     * @return String password
     */
    public String getPassword() {
        return password;
    }

    /**
     * 
     * @param String password 
     */
    public void setPassword(String password) {
        this.password = password;
    }
    
}