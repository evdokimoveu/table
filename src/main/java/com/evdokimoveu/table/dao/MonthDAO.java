
package com.evdokimoveu.table.dao;

import com.evdokimoveu.table.entity.Months;
import java.util.List;

/**
 *
 * @author Evdokymov Evgeny
 */
public interface MonthDAO {
    void updateMonth(Months months);
    void createNewMonthsTable(Months months);
    List<Months> getAllMonths();
    Months getMonthById(long id);
    int getSizeMonthTable();
    void addNewMonth(Months months);
}
