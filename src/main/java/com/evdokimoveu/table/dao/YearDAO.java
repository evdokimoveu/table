
package com.evdokimoveu.table.dao;

import com.evdokimoveu.table.entity.Years;
import java.util.List;

/**
 *
 * @author Evdokymov Evgeny
 */
public interface YearDAO {
    void addNewYear(Years years);
    List<Years> getAllYears();
    Years getYearById(long id);
    boolean checkCurrentYear(int year);
}
