

package com.evdokimoveu.table.dao;

import com.evdokimoveu.table.entity.Tabel;


/**
 *
 * @author Evdokymov Evgeny
 */
public interface TabelDAO {
    void addNewTabel(Tabel tabel);   
    void updateTabel(Tabel tabel);
    Tabel getTabelById(long id);
    Long getTabelId(long emploeeId, int year, int month);
}
