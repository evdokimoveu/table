
package com.evdokimoveu.table.dao.impl;

import com.evdokimoveu.table.dao.WorkDayDAO;
import com.evdokimoveu.table.entity.WorkDay;
import java.io.Serializable;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Evdokymov Evgeny
 */
@Repository
public class WorkDayDAOImpl implements WorkDayDAO, Serializable {
            
    private static final long serialVersionUID = 1L;
    
    private static final Logger log = Logger.getLogger(WorkDayDAOImpl.class);
    
    @Autowired
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void addNewWorkDay(WorkDay workDay) {
        sessionFactory.getCurrentSession().saveOrUpdate(workDay);
    }

    @Override
    public void updateWorkDay(WorkDay workDay) {
        sessionFactory.getCurrentSession().update(workDay);
    }

    @Override
    public List<WorkDay> getWorkDays(long id) {
        return this.sessionFactory.getCurrentSession()
                .createQuery("from WorkDay where ref_id_tabel = :id")
                .setLong("id", id).list();
    }

    @Override
    public WorkDay getWorkDayById(long id) {
        return (WorkDay) sessionFactory.getCurrentSession().get(WorkDay.class, id);
    }
}
