
package com.evdokimoveu.table.dao.impl;

import com.evdokimoveu.table.dao.EmploeeDAO;
import com.evdokimoveu.table.entity.Emploee;
import java.io.Serializable;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


/**
 *
 * @author Evdokymov Evgeny
 */

@Repository
public class EmploeeDAOImpl implements EmploeeDAO, Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private static final Logger log = Logger.getLogger(EmploeeDAOImpl.class);

    @Autowired
    private SessionFactory sessionFactory;
    
    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Override
    public void addNewEmploee(Emploee emploee) {
        sessionFactory.getCurrentSession().save(emploee);
    }

    @Override
    public void removeEmploee(long id) {
        Emploee emploee = getEmploeeById(id);
        if(emploee != null){
            sessionFactory.getCurrentSession().delete(emploee);
        }
    }

    @Override
    public void updateEmploee(Emploee emploee) {
        sessionFactory.getCurrentSession().update(emploee);
    }

    @Override
    public List<Emploee> getAllEmploee() {
        return this.sessionFactory.getCurrentSession().createQuery("from Emploee order by surname").list();
    }

    @Override
    public Emploee getEmploeeById(long id) {
        return (Emploee) sessionFactory.getCurrentSession().get(Emploee.class, id);
    }
    
    /**
     * 
     * @param id Emploee 
     * @param count 
     * @return 
     */
    @Override
    public int changeLeaveCount(long id, int count) {
        String strQuery = "update Emploee set leaveCount = leaveCount + :count where id = :id";
        Query query = sessionFactory.getCurrentSession().createQuery(strQuery);
        query.setLong("id", id);
        query.setInteger("count", count);
        return query.executeUpdate();
    }


}
