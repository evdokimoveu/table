
package com.evdokimoveu.table.dao.impl;

import com.evdokimoveu.table.dao.ProfessionCodeDAO;
import com.evdokimoveu.table.entity.ProfessionCode;
import java.io.Serializable;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Evdokymov Evgeny
 */

@Repository
public class ProfessoinCodeDAOImpl implements ProfessionCodeDAO, Serializable {
            
    private static final long serialVersionUID = 1L;
    
    private static final Logger log = Logger.getLogger(ProfessoinCodeDAOImpl.class);

    @Autowired
    private SessionFactory sessionFactory;  

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Override
    public void addNewProfCode(ProfessionCode professionCode) {
        sessionFactory.getCurrentSession().save(professionCode);
    }

    @Override
    public void removeProfCode(long id) {
        ProfessionCode professionCode = getProfessoinCodeById(id);
        if(professionCode != null){
            sessionFactory.getCurrentSession().delete(professionCode);
        }
    }

    @Override
    public void updateProfCode(ProfessionCode professionCode) {
        sessionFactory.getCurrentSession().update(professionCode);
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public List<ProfessionCode> getAllProfessoinCode() {
        return sessionFactory.getCurrentSession().createQuery("FROM ProfessionCode order by name").list();
    }

    @Override
    public ProfessionCode getProfessoinCodeById(long id) {
        return (ProfessionCode) sessionFactory.getCurrentSession().get(ProfessionCode.class, id);
    }

}
