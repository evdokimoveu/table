
package com.evdokimoveu.table.dao.impl;

import com.evdokimoveu.table.dao.YearDAO;
import com.evdokimoveu.table.entity.Years;
import java.io.Serializable;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Evdokymov Evgeny
 */
@Repository
public class YearDAOImpl implements YearDAO, Serializable {

    private static final long serialVersionUID = 1L;
    
    private static final Logger log = Logger.getLogger(YearDAOImpl.class);
        
    @Autowired
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }        
    
    @Override
    public void addNewYear(Years years) {
        sessionFactory.getCurrentSession().save(years);
    }

    @Override
    public List<Years> getAllYears() {
        return this.sessionFactory.getCurrentSession().createQuery("from Years order by name").list();
    }

    @Override
    public Years getYearById(long id) {
        return (Years) sessionFactory.getCurrentSession().get(Years.class, id);
    }

    /*Check */
    @Override
    public boolean checkCurrentYear(int year) {
        int sizeYear = sessionFactory.getCurrentSession().createQuery("from Years where name = '"+year+"'").list().size();
        if(sizeYear == 0){
            return false;
        }
        else { 
            return true;
        }
    }
    
}
