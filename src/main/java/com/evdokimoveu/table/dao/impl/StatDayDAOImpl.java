
package com.evdokimoveu.table.dao.impl;

import com.evdokimoveu.table.dao.StatDayDAO;
import com.evdokimoveu.table.entity.StatDay;
import java.io.Serializable;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Evdokymov Evgeny
 */

@Repository
public class StatDayDAOImpl implements StatDayDAO, Serializable {

    private static final long serialVersionUID = 1L;
    
    private static final Logger log = Logger.getLogger(StatDayDAOImpl.class);
    
    @Autowired
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void addNewStatDay(StatDay statDay) {
        sessionFactory.getCurrentSession().save(statDay);
    }

    @Override
    public void removeStatDay(long id) {
        StatDay statDay = getStatDayById(id);
        if(statDay != null){
            sessionFactory.getCurrentSession().delete(statDay);
        }
    }

    @Override
    public void updateStatDay(StatDay statDay) {
        sessionFactory.getCurrentSession().update(statDay);
    }


    @Override
    @SuppressWarnings("unchecked")
    public List<StatDay> getAllStatDay() {
        return sessionFactory.getCurrentSession().createQuery("FROM StatDay order by name").list();
    }

    @Override
    public StatDay getStatDayById(long id) {
        return (StatDay) sessionFactory.getCurrentSession().get(StatDay.class, id);
    }

}