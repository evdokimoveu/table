
package com.evdokimoveu.table.dao.impl;

import com.evdokimoveu.table.dao.MonthDAO;
import com.evdokimoveu.table.entity.Months;
import java.io.Serializable;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Evdokymov Evgeny
 */
@Repository
public class MonthDAOImpl implements MonthDAO, Serializable {

    private static final long serialVersionUID = 1L;
    
    private static final Logger log = Logger.getLogger(MonthDAO.class);
    
    @Autowired
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
        
    @Override
    public void updateMonth(Months months) {
        sessionFactory.getCurrentSession().update(months);
    }

    @Override
    public void createNewMonthsTable(Months months) {
        sessionFactory.getCurrentSession().save(months);        
    }

    @Override
    public List<Months> getAllMonths() {
        return this.sessionFactory.getCurrentSession().createQuery("from Months order by id_months").list();
    }

    @Override
    public Months getMonthById(long id) {
        return (Months) sessionFactory.getCurrentSession().get(Months.class, id);
    }

    @Override
    public int getSizeMonthTable() {
        return sessionFactory.getCurrentSession().createQuery("from Months").list().size();
    }

    @Override
    public void addNewMonth(Months months) {
        sessionFactory.getCurrentSession().save(months);
    }
    
}
