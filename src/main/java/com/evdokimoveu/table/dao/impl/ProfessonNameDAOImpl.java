
package com.evdokimoveu.table.dao.impl;

import com.evdokimoveu.table.dao.ProfessionNameDAO;
import com.evdokimoveu.table.entity.ProfessionName;
import java.io.Serializable;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Evdokymov Evgeny
 */

@Repository
public class ProfessonNameDAOImpl implements ProfessionNameDAO, Serializable {

    private static final long serialVersionUID = 1L;
    
    private static final Logger log = Logger.getLogger(ProfessonNameDAOImpl.class);
    
    @Autowired
    private SessionFactory sessionFactory;
    
    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    @Override
    public void addNewProfName(ProfessionName professionName) {
        sessionFactory.getCurrentSession().save(professionName);
    }

    @Override
    public void removeProfName(long id) {
        ProfessionName professionName = getProfessionNameById(id);
        if(professionName != null){
            sessionFactory.getCurrentSession().delete(professionName);
        }
    }

    @Override
    public void updateProfName(ProfessionName professionName) {
        sessionFactory.getCurrentSession().update(professionName);
    }   
    
    @Override
    public List<ProfessionName> getAllProfessoinName() {
        return this.sessionFactory.getCurrentSession().createQuery("from ProfessionName order by name").list();
    }

    @Override
    public ProfessionName getProfessionNameById(long id) {
        return (ProfessionName) sessionFactory.getCurrentSession().get(ProfessionName.class, id);
    }
    
    
}
