
package com.evdokimoveu.table.dao.impl;

import com.evdokimoveu.table.dao.TabelDAO;
import com.evdokimoveu.table.entity.Tabel;
import java.io.Serializable;
import java.util.Calendar;
import java.util.GregorianCalendar;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Evdokymov Evgeny
 */
@Repository
public class TabelDAOImpl implements TabelDAO, Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private static final Logger log = Logger.getLogger(TabelDAOImpl.class);
    
    @Autowired
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void addNewTabel(Tabel tabel) {
        sessionFactory.getCurrentSession().save(tabel);
    }

    @Override
    public void updateTabel(Tabel tabel) {
        sessionFactory.getCurrentSession().saveOrUpdate(tabel);
    }

    @Override
    public Tabel getTabelById(long id) {
        return (Tabel) sessionFactory.getCurrentSession().get(Tabel.class, id);
    }

    @Override
    public Long getTabelId(long emploeeId, int year, int month) {
        Calendar calendar = new GregorianCalendar();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        return (Long)sessionFactory.getCurrentSession()
                .createQuery("select t.id from Tabel t where t.emploee = :emploeeId and t.dateCalendar = :date")
                .setLong("emploeeId", emploeeId)
                .setDate("date", calendar.getTime()).uniqueResult();
    }
    
}
