
package com.evdokimoveu.table.dao;

import com.evdokimoveu.table.entity.WorkDay;
import java.util.List;

/**
 *
 * @author Evdokymov Evgeny
 */
public interface WorkDayDAO {
    void addNewWorkDay(WorkDay workDay);    
    void updateWorkDay(WorkDay workDay);
    List<WorkDay> getWorkDays(long id);
    WorkDay getWorkDayById(long id);
}
