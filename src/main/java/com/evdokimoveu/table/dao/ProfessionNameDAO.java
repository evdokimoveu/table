
package com.evdokimoveu.table.dao;

import com.evdokimoveu.table.entity.ProfessionName;
import java.util.List;

/**
 *
 * @author Evdokymov Evgeny
 */
public interface ProfessionNameDAO {
    public void addNewProfName(ProfessionName professionName);
    public void removeProfName(long id);
    public void updateProfName(ProfessionName professionName);
    public List<ProfessionName> getAllProfessoinName();
    public ProfessionName getProfessionNameById(long id);
}
