
package com.evdokimoveu.table.dao;

import com.evdokimoveu.table.entity.ProfessionCode;
import java.util.List;

/**
 *
 * @author Evdokymov Evgeny
 */
public interface ProfessionCodeDAO {
    public void addNewProfCode(ProfessionCode professionCode);
    public void removeProfCode(long id);
    public void updateProfCode(ProfessionCode professionCode);
    public List<ProfessionCode> getAllProfessoinCode();
    public ProfessionCode getProfessoinCodeById(long id);
}
