
package com.evdokimoveu.table.dao;

import com.evdokimoveu.table.entity.Emploee;
import java.util.List;


/**
 *
 * @author Evdokymov Evgeny
 */
public interface EmploeeDAO {
    void addNewEmploee(Emploee emploee);
    void removeEmploee(long id);
    void updateEmploee(Emploee emploee);
    List<Emploee> getAllEmploee();
    Emploee getEmploeeById(long id);
    int changeLeaveCount(long id, int count);
}
