
package com.evdokimoveu.table.dao;

import com.evdokimoveu.table.entity.StatDay;
import java.util.List;

/**
 *
 * @author Evdokymov Evgeny
 */
public interface StatDayDAO {
    public void addNewStatDay(StatDay statDay);
    public void removeStatDay(long id);
    public void updateStatDay(StatDay statDay);
    public List<StatDay> getAllStatDay();
    public StatDay getStatDayById(long id);
}
