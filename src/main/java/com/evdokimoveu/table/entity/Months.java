
package com.evdokimoveu.table.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Evdokymov Evgeny
 */
@Entity
@Table(name="months")
public class Months implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_months", nullable = false, unique = true) 
    private Long id_months;

    @Column(name="name", length = 20, nullable = false)
    private String name;
    
    public Long getId_months() {
        return id_months;
    }

    public void setId_months(Long id_months) {
        this.id_months = id_months;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    @Override
    public String toString() {
        StringBuffer strBuff = new StringBuffer();
        strBuff.append(" id : ").append(getId_months());
        strBuff.append(", Год : ").append(getName());
        return strBuff.toString();
    }
    
    @Override
    public boolean equals(Object obj) {
        if(obj == null){
            return false;            
        }
        if(this == obj){
            return true;
        }
        if(getClass() != obj.getClass()){
            return false;
        }
        Months months = (Months) obj;
        return months.getId_months() == id_months &&
                months.getName().equals(name);
    }
    
    @Override
    public int hashCode() {
        int result  = 17;
        result = 31 * result + (int)(id_months - (id_months >>> 32));
        result = 31 * result + (name == null ? 0 : name.hashCode());
        return result;
    }    
}
