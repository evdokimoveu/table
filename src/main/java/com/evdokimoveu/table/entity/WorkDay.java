
package com.evdokimoveu.table.entity;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author Evdokymov Evgeny
 */
@Entity
@Table(name="workdays")
public class WorkDay implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_workdays", nullable=false)
    private long id;
    
    @Column(name="day_of_month", length=2, nullable=false)
    private int dayOfMonth;
    
    @Column(name="hours", length=2, nullable=false)
    private int hours;
            
    @ManyToOne(cascade = {CascadeType.REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = "ref_id_statday", nullable=false)
    private StatDay statDay;
    
    @ManyToOne(cascade = {CascadeType.REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = "ref_id_tabel", nullable=false)
    private Tabel tabel;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getDayOfMonth() {
        return dayOfMonth;
    }

    public void setDayOfMonth(int dayOfMonth) {
        this.dayOfMonth = dayOfMonth;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public StatDay getStatDay() {
        return statDay;
    }

    public void setStatDay(StatDay statDay) {
        this.statDay = statDay;
    }

    public Tabel getTabel() {
        return tabel;
    }

    public void setTabel(Tabel tabel) {
        this.tabel = tabel;
    }
        
    @Override
    public String toString() {
        StringBuffer strBuff = new StringBuffer();
        strBuff.append(" id : ").append(getId());
        strBuff.append(" День месяца : ").append(getDayOfMonth());
        strBuff.append(" Отработано часов : ").append(getHours());        
        return strBuff.toString();
    }
    
     @Override
    public boolean equals(Object obj) {
        if(obj == null){
            return false;            
        }
        if(this == obj){
            return true;
        }
        if(getClass() != obj.getClass()){
            return false;
        }
        WorkDay workDay = (WorkDay) obj;
        return workDay.getId() == id &&
                workDay.getHours() == hours &&
                workDay.getDayOfMonth() == dayOfMonth &&                
                workDay.getStatDay().equals(statDay) &&
                workDay.getTabel().equals(tabel);
    }
    
    @Override
    public int hashCode() {
        int result  = 17;

        result = 31 * result + (int)(id - (id >>> 32));
        result = 31 * result + hours;
        result = 31 * result + dayOfMonth;
        result = 31 * result + (statDay == null ? 0 : statDay.hashCode());
        result = 31 * result + (tabel == null ? 0 : tabel.hashCode());
        
        return result;
    }
}
