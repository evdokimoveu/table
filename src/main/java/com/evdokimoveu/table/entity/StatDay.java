
package com.evdokimoveu.table.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Evdokymov Evgeny
 */

@Entity
@Table(name="statday")
public class StatDay implements Serializable {
        
    private static final long serialVersionUID = 1L;
    
    @Id 
    @Column(name = "id_statday", nullable = false, unique = true)
    @GeneratedValue(strategy= GenerationType.IDENTITY)    
    private long id;

    @Column(name = "name", length = 20, nullable = false, unique = true)
    private String name;

    @Column(name = "description", length = 20, nullable = false, unique = true)
    private String description;    
       
    @OneToMany(cascade={CascadeType.ALL}, mappedBy = "statDay")
    private List<WorkDay> workDays; 

    public void setId(long id) {
        this.id = id;
    }
    
    public long getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<WorkDay> getWorkDays() {
        return workDays;
    }

    public void setWorkDays(List<WorkDay> workDays) {
        this.workDays = workDays;
    }
    

    @Override
    public String toString() {
        StringBuffer strBuff = new StringBuffer();
        strBuff.append(" id : ").append(getId());
        strBuff.append(", статус : ").append(getName());
        strBuff.append(" - ").append(getDescription());
        return strBuff.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null){
            return false;
        }       
        if(this == obj){
            return true;
        }
        if(getClass() != obj.getClass()){
            return false;
        }
        StatDay statDay = (StatDay) obj;
        return statDay.getId() == id &&
                statDay.getName().equals(name) &&
                statDay.getWorkDays().equals(workDays) &&
                statDay.getDescription().equals(description);
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + (int)(id - (id >>> 32));
        result = 31 * result + (name == null ? 0 : name.hashCode());
        result = 31 * result + (description == null ? 0 : description.hashCode());
        if(workDays != null){
            for(int i = 0; i < workDays.size(); i++){
                result = 31 * result + (workDays.get(i) == null ? 0: workDays.get(i).hashCode());
            }
        }   
        return result;    
    }
   
}
