
package com.evdokimoveu.table.entity;


import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * 
 * @author Evdokymov Evgeny
 */
@Entity
@Table(name = "emploee")
public class Emploee implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_emploee", nullable = false, unique = true) 
    private long id;
    
    @Column(name="name", length = 20, nullable = false)
    private String name;
    
    @Column(name="surname", length = 20, nullable = false)
    private String surname;
    
    @Column(name="patronomic", length = 20, nullable = true)
    private String patronomic;
    
    @Column(name="leave_count", nullable = false)
    private int leaveCount;
    
    @Column(name="salary", nullable = true)
    private int salary;
    
    @Column(name="sex", nullable=false)
    private String sex;
    
    @Column(name="tabelnum", nullable=false)
    private String tabelnum;
    
    @ManyToOne(cascade = {CascadeType.REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = "ref_id_professionname", nullable = false)
    private ProfessionName emploeeProfName;
    
    @OneToMany(cascade={CascadeType.ALL}, mappedBy = "emploee")
    private List<Tabel> tabelList;    

    
    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getSurname() {
        return surname;
    }

    public void setPatronomic(String patronomic) {
        this.patronomic = patronomic;
    }

    public String getPatronomic() {
        return patronomic;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }
    
    public int getSalary() {
        return salary;
    }
    
    public void setLeaveCount(int leaveCount) {
        this.leaveCount = leaveCount;
    }

    public int getLeaveCount() {
        return leaveCount;
    }

    public void setProfName(ProfessionName profName) {
        this.emploeeProfName = profName;
    }

    public ProfessionName getProfName() {
        return emploeeProfName;
    }

    public List<Tabel> getTabelList() {
        return tabelList;
    }

    public void setTabelList(List<Tabel> tabelList) {
        this.tabelList = tabelList;
    }    
    
    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getTabelnum() {
        return tabelnum;
    }

    public void setTabelnum(String tabelnum) {
        this.tabelnum = tabelnum;
    }   
    
    
    @Override
    public String toString() {
        StringBuffer strBuff = new StringBuffer();
        strBuff.append(" id : ").append(getId());
        strBuff.append(" Табельний номер : ").append(getTabelnum());
        strBuff.append(" Пол : ").append(getSex());
        strBuff.append(", Фамилия : ").append(getSurname());
        strBuff.append(", Имя : ").append(getName());
        strBuff.append(", Отчество : ").append(getPatronomic());
        strBuff.append(", Професия : ").append(getProfName());
        strBuff.append(", Зарплата : ").append(getSalary());
        strBuff.append(", Не использованых дней отпуска : ").append(getLeaveCount());
        return strBuff.toString();
    }
    
    @Override
    public boolean equals(Object obj) {
        if(obj == null){
            return false;            
        }
        if(this == obj){
            return true;
        }
        if(getClass() != obj.getClass()){
            return false;
        }
        Emploee emploee = (Emploee) obj;
        return emploee.getId() == id &&
                emploee.getName().equals(name) &&
                emploee.getSurname().equals(surname) &&
                emploee.getPatronomic().equals(patronomic) &&
                emploee.getSalary() == salary &&
                emploee.getLeaveCount() == leaveCount &&
                emploee.getProfName().equals(emploeeProfName) &&
                emploee.getTabelList().equals(tabelList);
    }
    
    @Override
    public int hashCode() {
        int result  = 17;

        result = 31 * result + (int)(id - (id >>> 32));
        result = 31 * result + (name == null ? 0 : name.hashCode());
        result = 31 * result + (surname == null ? 0 : surname.hashCode());
        result = 31 * result + (patronomic == null ? 0 : patronomic.hashCode());
        result = 31 * result + leaveCount;
        result = 31 * result + salary;
        result = 31 * result + (emploeeProfName == null ? 0 : emploeeProfName.hashCode());
        if(tabelList != null){
            for(int i = 0; i < tabelList.size(); i++){
                result = 31 * result + (tabelList.get(i) == null ?  0 : tabelList.get(i).hashCode());
            }
        }
        return result;
    }
}