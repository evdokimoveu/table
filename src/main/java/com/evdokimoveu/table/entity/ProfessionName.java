
package com.evdokimoveu.table.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Evdokymov Evgeny
 */

@Entity
@Table(name = "professionname")
public class ProfessionName implements Serializable{

    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name = "id_professionname", nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @Column(name = "name", length = 200, nullable = false)
    private String name;
    
    @ManyToOne(cascade = {CascadeType.REFRESH})
    @JoinColumn(name = "ref_id_professioncode", nullable = false)
    private ProfessionCode profCode;
    
    @OneToMany(cascade = {CascadeType.ALL}, mappedBy = "emploeeProfName")    
    private List<Emploee> listEmploeeProfName;
    

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setProfCode(ProfessionCode profCode) {
        this.profCode = profCode;
    }

    public ProfessionCode getProfCode() {
        return profCode;
    }

    public List<Emploee> getListEmploeeProfName() {
        return listEmploeeProfName;
    }

    public void setListEmploeeProfName(List<Emploee> listEmploeeProfName) {
        this.listEmploeeProfName = listEmploeeProfName;
    }

    @Override
    public String toString() {
        StringBuffer strBuff = new StringBuffer();
        strBuff.append(" id : ").append(getId());
        strBuff.append(", Профессия : ").append(getName());
        strBuff.append(", код : ").append(getProfCode());
        return strBuff.toString();                
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null){
            return false;
        }
        if(this == obj){
            return true;
        }
        if(getClass() != obj.getClass()){
            return false;
        }
        ProfessionName professionName = (ProfessionName) obj;
        
        return professionName.getId() == id &&
                professionName.getName().equals(name) &&
                professionName.getProfCode().equals(profCode);
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + (int)(id - (id >>> 32));
        result = 31 * result + (name == null ? 0 : name.hashCode());
        result = 31 * result + (profCode == null ? 0 : profCode.hashCode());
        return result;
    }

}
