
package com.evdokimoveu.table.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Evdokymov Evgeny
 */

@Entity
@Table(name = "professioncode")
public class ProfessionCode implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name = "id_professioncode", nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @Column(name = "name", length = 20, nullable = false, unique = true)
    private String name;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "profCode")
    private List<ProfessionName> profNames;


    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setProfNames(List<ProfessionName> profNames) {
        this.profNames = profNames;
    }

    public List<ProfessionName> getProfNames() {
        return profNames;
    }
    

    @Override
    public boolean equals(Object obj) {
        if(obj == null){
            return false;
        }
        if(this == obj){
            return true;
        }
        if(getClass() != obj.getClass()){
            return false;
        }
        ProfessionCode profCode = (ProfessionCode) obj;
        return profCode.getId() == id &&
                profCode.getName().equals(name) &&
                profCode.getProfNames().equals(profNames);        
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + (int)(id - (id >>> 32));
        result = 31 * result + (name == null ? 0 : name.hashCode());
        if(profNames != null){
            for(int i = 0; i < profNames.size(); i++){
                result = 31 * result + (profNames.get(i) == null ? 0 : profNames.get(i).hashCode());
            }
        }
        return result;
    }

    @Override
    public String toString() {
        StringBuffer strBuff = new StringBuffer();
        strBuff.append(" id : ").append(getId());
        strBuff.append(", Код профессии : ").append(getName());
        return strBuff.toString();
    }
   
}
