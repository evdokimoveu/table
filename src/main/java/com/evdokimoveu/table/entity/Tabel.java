

package com.evdokimoveu.table.entity;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Evdokymov Evgeny
 */

@Entity
@Table(name="tabel")
public class Tabel implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_tabel", nullable = false)
    private long id;
    
    @Column(name = "tabel_date", nullable = false)
    @Temporal(TemporalType.DATE)
    private Calendar dateCalendar;
  
    @Column(name="accrued_leave", nullable=false, length=1)
    private int accruedLeave;
    
    @ManyToOne(cascade = {CascadeType.REFRESH})
    @JoinColumn(name = "ref_id_emploee")
    private Emploee emploee;
    
    @OneToMany(cascade={CascadeType.ALL}, mappedBy = "tabel")
    private List<WorkDay> workDays; 
            
    
    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setDateCalendar(Calendar dateCalendar) {
        this.dateCalendar = dateCalendar;
    }

    public Calendar getDateCalendar() {
        return dateCalendar;
    }

    public int getAccruedLeave() {
        return accruedLeave;
    }

    public void setAccruedLeave(int accruedLeave) {
        this.accruedLeave = accruedLeave;
    }    

    public List<WorkDay> getWorkDays() {
        return workDays;
    }

    public void setWorkDays(List<WorkDay> workDays) {
        this.workDays = workDays;
    }

    public Emploee getEmploee() {
        return emploee;
    }

    public void setEmploee(Emploee emploee) {
        this.emploee = emploee;
    }
    
    
    @Override
    public String toString() {        
        SimpleDateFormat format = new SimpleDateFormat("dd.mm.yyyy"); 
        return format.format(dateCalendar.getTime()) + " : " + emploee + " \n";        
    }
    
     @Override
    public boolean equals(Object obj) {
        if(obj == null){
            return false;            
        }
        if(this == obj){
            return true;
        }
        if(getClass() != obj.getClass()){
            return false;
        }
        Tabel tabel = (Tabel) obj;
        return tabel.getId() == id &&
                tabel.getDateCalendar().equals(dateCalendar) &&
                tabel.getAccruedLeave() == accruedLeave &&
                tabel.getWorkDays().equals(workDays) &&                
                tabel.getEmploee().equals(emploee);
    }
    
    @Override
    public int hashCode() {
        int result  = 17;

        result = 31 * result + (int)(id - (id >>> 32));
        result = 31 * result + accruedLeave;
        result = 31 * result + (emploee == null ? 0 : emploee.hashCode());
        result = 31 * result + (dateCalendar == null ? 0 : dateCalendar.hashCode());
        if(workDays != null){
            for(int i = 0; i < workDays.size(); i++){
                result = 31 * result + (workDays.get(i) == null ?  0 : workDays.get(i).hashCode());
            }
        }
        return result;
    }
}
