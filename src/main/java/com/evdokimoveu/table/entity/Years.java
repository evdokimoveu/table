
package com.evdokimoveu.table.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Evdokymov Evgeny
 */
@Entity
@Table(name="years")
public class Years implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_years", nullable = false, unique = true) 
    private Long id;

    @Column(name="name", length = 4, nullable = false)
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
        
    @Override
    public String toString() {
        StringBuffer strBuff = new StringBuffer();
        strBuff.append(" id : ").append(getId());
        strBuff.append(", Год : ").append(getName());
        return strBuff.toString();
    }
    
    @Override
    public boolean equals(Object obj) {
        if(obj == null){
            return false;            
        }
        if(this == obj){
            return true;
        }
        if(getClass() != obj.getClass()){
            return false;
        }
        Years years = (Years) obj;
        return years.getId() == id &&
                years.getName().equals(name);
    }
    
    @Override
    public int hashCode() {
        int result  = 17;

        result = 31 * result + (int)(id - (id >>> 32));
        result = 31 * result + (name == null ? 0 : name.hashCode());        
        return result;
    }
}
