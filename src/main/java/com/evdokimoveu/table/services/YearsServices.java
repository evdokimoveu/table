
package com.evdokimoveu.table.services;

import com.evdokimoveu.table.entity.Years;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Evdokymov Evgeny
 */
public interface YearsServices {
    void addNewYear();
    List<Years> getAllYears();
    Years getYearById(long id);
    Map<Long, String> getAllYearsLikeMap();    
}
