
package com.evdokimoveu.table.services.impl;

import com.evdokimoveu.table.dao.MonthDAO;
import com.evdokimoveu.table.entity.Months;
import com.evdokimoveu.table.services.MonthsServices;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Evdokymov Evgeny
 */
@Service(value="MonthsServicesImpl")
@Transactional
public class MonthsServicesImpl implements MonthsServices, Serializable {

    private static final long serialVersionUID = 1L;
    
    private static final Logger log = Logger.getLogger(MonthsServicesImpl.class);
    
    @Autowired
    MonthDAO monthDAO;
        
    @Override
    @Transactional
    public void updateMonth(Months months) {
        monthDAO.updateMonth(months);
    }

    @Override
    @Transactional
    public void createNewMonthsTable() {
        int sizeMonthTable = monthDAO.getSizeMonthTable();
        if(sizeMonthTable == 0){
            for(long i = 1; i <= 12; i++){
                Months months = new Months();
                months.setId_months(i);
                months.setName("");
                monthDAO.addNewMonth(months);
            }
        }
    }

    @Override
    public List<Months> getAllMonths() {
        return monthDAO.getAllMonths();
    }

    @Override
    public Months getMonthById(long id) {
        return monthDAO.getMonthById(id);
    }
    
    @Override
    public Map<Long, String> getAllMonthsLikeMap() {
        List<Months> months = monthDAO.getAllMonths();
        Map<Long, String> monthsMap = new LinkedHashMap<Long, String>();
        for (Months month: months) {
            monthsMap.put(new Long(month.getId_months()), month.getName());
        }
        return monthsMap;
    }
    
}
