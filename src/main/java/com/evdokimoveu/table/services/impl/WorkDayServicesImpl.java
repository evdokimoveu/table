
package com.evdokimoveu.table.services.impl;

import com.evdokimoveu.table.dao.EmploeeDAO;
import com.evdokimoveu.table.dao.TabelDAO;
import com.evdokimoveu.table.dao.WorkDayDAO;
import com.evdokimoveu.table.entity.Emploee;
import com.evdokimoveu.table.entity.Tabel;
import com.evdokimoveu.table.entity.WorkDay;
import com.evdokimoveu.table.services.WorkDayServices;
import java.io.Serializable;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Evdokymov Evgeny
 */
@Service(value="WorkDayServicesImpl")
@Transactional
public class WorkDayServicesImpl implements WorkDayServices, Serializable {

    private static final long serialVersionUID = 1L;
    
    private static final Logger log = Logger.getLogger(WorkDayServicesImpl.class);
    
    @Autowired
    WorkDayDAO workDayDAO;
        
    @Autowired
    EmploeeDAO emploeeDAO;
    
    @Autowired
    TabelDAO tabelDAO;
    
    
    @Override
    @Transactional
    public void addNewWorkDay(WorkDay workDay) {
        workDayDAO.addNewWorkDay(workDay);
    }

    @Override
    @Transactional
    public void updateWorkDay(WorkDay workDay) {
        workDayDAO.updateWorkDay(workDay);
    }

    @Override
    public List<WorkDay> getWorkDays(long id) {
        return workDayDAO.getWorkDays(id);
    }

    @Override
    public WorkDay getWorkDayById(long id) {
        return workDayDAO.getWorkDayById(id);
    }

    @Override
    @Transactional
    public void addWorkDayByPackage(long emploeeId, List<WorkDay> tabels, int year, int month) {
        emploeeDAO.changeLeaveCount(emploeeId, 2);
        Long tableId = tabelDAO.getTabelId(emploeeId, year, month);
        Tabel tabel;
        if(tableId != null){
            tabel = tabelDAO.getTabelById(tableId);
        }
        else{
            tabel = new Tabel();
            Calendar calendar = GregorianCalendar.getInstance();
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month);
            tabel.setDateCalendar(calendar);
            tabel.setEmploee(emploeeDAO.getEmploeeById(emploeeId));            
        }
        tabel.setAccruedLeave(1);
        tabelDAO.addNewTabel(tabel);        
        for(int i = 0; i < tabels.size(); i++){
            WorkDay workDay = tabels.get(i);
            workDay.setTabel(tabel);            
            addNewWorkDay(workDay);
        }
    }

}
