
package com.evdokimoveu.table.services.impl;

import com.evdokimoveu.table.dao.ProfessionCodeDAO;
import com.evdokimoveu.table.entity.ProfessionCode;
import com.evdokimoveu.table.services.ProfessionCodeServices;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Evdokymov Evgeny
 */

@Service(value="ProfessionCodeServicesImpl")
@Transactional
public class ProfessionCodeServicesImpl implements ProfessionCodeServices, Serializable {

    private static final long serialVersionUID = 1L;
    
    private static final Logger log = Logger.getLogger(ProfessionCodeServicesImpl.class);
    
    @Autowired
    ProfessionCodeDAO professionCodeDAO;

    @Override
    @Transactional
    public void addNewProfCode(ProfessionCode professionCode) {
        professionCodeDAO.addNewProfCode(professionCode);
    }

    @Override
    @Transactional
    public void removeProfCode(long id) {
        professionCodeDAO.removeProfCode(id);
    }

    @Override
    @Transactional
    public void updateProfCode(ProfessionCode professionCode) {
        professionCodeDAO.updateProfCode(professionCode);
    }

    @Override    
    public List<ProfessionCode> getAllProfessoinCode() {
        return professionCodeDAO.getAllProfessoinCode();
    }

    @Override    
    public ProfessionCode getProfessoinCodeById(long id) {
        return professionCodeDAO.getProfessoinCodeById(id);
    }    

    public ProfessionCodeDAO getProfessionCodeDAO() {
        return professionCodeDAO;
    }

    public void setProfessionCodeDAO(ProfessionCodeDAO professionCodeDAO) {
        this.professionCodeDAO = professionCodeDAO;
    }    

    @Override
    public Map<Long, String> getAllProfessionCodeLikeMap() {
        List<ProfessionCode> profCodes = professionCodeDAO.getAllProfessoinCode();
        Map<Long, String> profCode = new HashMap<Long, String>();
        for (ProfessionCode pfCode: profCodes) {
            profCode.put(new Long(pfCode.getId()), pfCode.getName());
        }
        return profCode;
    }

}
