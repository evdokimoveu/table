
package com.evdokimoveu.table.services.impl;

import com.evdokimoveu.table.dao.TabelDAO;
import com.evdokimoveu.table.entity.Tabel;
import com.evdokimoveu.table.services.TabelServices;
import java.io.Serializable;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Evdokymov Evgeny
 */
@Service(value="TabelServicesImpl")
@Transactional
public class TabelServicesImpl implements TabelServices, Serializable {
        
    private static final long serialVersionUID = 1L;
    
    private static final Logger log = Logger.getLogger(TabelServicesImpl.class);
    
    @Autowired
    TabelDAO tabelDAO;

    @Override
    @Transactional
    public void addNewTabel(Tabel tabel) {
        tabelDAO.addNewTabel(tabel);
    }

    @Override
    @Transactional
    public void updateTabel(Tabel tabel) {
        tabelDAO.updateTabel(tabel);
    }

    @Override
    public Tabel getTabelById(long id) {
        return tabelDAO.getTabelById(id);
    }

    @Override
    public Long getTabelId(long emploeeId, int year, int month) {
        return tabelDAO.getTabelId(emploeeId, year, month);
    }
    
}
