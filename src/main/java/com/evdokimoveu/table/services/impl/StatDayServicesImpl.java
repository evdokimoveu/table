
package com.evdokimoveu.table.services.impl;

import com.evdokimoveu.table.dao.StatDayDAO;
import com.evdokimoveu.table.entity.StatDay;
import com.evdokimoveu.table.services.StatDayServices;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Evdokymov Evgeny
 */


@Service(value="StatDayServicesImpl")
@Transactional
public class StatDayServicesImpl implements StatDayServices, Serializable {

    private static final long serialVersionUID = 1L;
    
    private static final Logger log = Logger.getLogger(StatDayServicesImpl.class);
    
    @Autowired
    StatDayDAO statDayDAO;
    
    @Override
    @Transactional
    public void addNewStatDay(StatDay statDay) {
        statDayDAO.addNewStatDay(statDay);
    }

    @Override
    @Transactional
    public void removeStatDay(long id) {
        statDayDAO.removeStatDay(id);
    }

    @Override
    @Transactional
    public void updateStatDay(StatDay statDay) {
        statDayDAO.updateStatDay(statDay);
    }

    @Override   
    public List<StatDay> getAllStatDay() {
        return statDayDAO.getAllStatDay();
    }

    @Override
    public StatDay getStatDayById(long id) {
        return statDayDAO.getStatDayById(id);
    }

    public StatDayDAO getStatDayDAO() {
        return statDayDAO;
    }

    public void setStatDayDAO(StatDayDAO statDayDAO) {
        this.statDayDAO = statDayDAO;
    }

    @Override
    public Map<Long, String> getAllStatDayLikeMap() {
        List<StatDay> statDays = statDayDAO.getAllStatDay();
        Map<Long, String> statDayMap = new LinkedHashMap<Long, String>();
        for (StatDay statDay: statDays) {
            statDayMap.put(new Long(statDay.getId()), statDay.getName());
        }
        return statDayMap;
    }
    
}
