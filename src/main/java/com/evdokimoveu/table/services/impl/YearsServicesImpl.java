
package com.evdokimoveu.table.services.impl;

import com.evdokimoveu.table.dao.YearDAO;
import com.evdokimoveu.table.entity.Years;
import com.evdokimoveu.table.services.YearsServices;
import java.io.Serializable;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Evdokymov Evgeny
 */

@Service(value="YearsServicesImpl")
@Transactional
public class YearsServicesImpl implements YearsServices, Serializable {
        
    private static final long serialVersionUID = 1L;
    
    private static final Logger log = Logger.getLogger(YearsServicesImpl.class);
    
    @Autowired
    YearDAO yearDAO; 
    
    @Override
    @Transactional
    public void addNewYear() {
        Calendar calendar = new GregorianCalendar();
        int currentYear = calendar.get(Calendar.YEAR);
        if(!yearDAO.checkCurrentYear(currentYear)){
            Years year = new Years();
            year.setName(new Integer(currentYear).toString());
            yearDAO.addNewYear(year);
        }
    }

    @Override
    public List<Years> getAllYears() {
        return yearDAO.getAllYears();
    }

    @Override
    public Years getYearById(long id) {
        return yearDAO.getYearById(id);
    }

    @Override
    public Map<Long, String> getAllYearsLikeMap() {
        List<Years> years = yearDAO.getAllYears();
        Map<Long, String> yearsMap = new LinkedHashMap<Long, String>();
        for (Years year: years) {
            yearsMap.put(new Long(year.getId()), year.getName());
        }
        return yearsMap;
    }    
}
