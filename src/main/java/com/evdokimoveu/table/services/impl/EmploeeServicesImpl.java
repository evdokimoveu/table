
package com.evdokimoveu.table.services.impl;

import com.evdokimoveu.table.dao.EmploeeDAO;
import com.evdokimoveu.table.entity.Emploee;
import com.evdokimoveu.table.services.EmploeeServices;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Evdokymov Evgeny
 */

@Service(value="EmploeeServicesImpl")
@Transactional
public class EmploeeServicesImpl implements EmploeeServices, Serializable  {
    
    private static final long serialVersionUID = 1L;
    
    private static final Logger log = Logger.getLogger(EmploeeServicesImpl.class);

    @Autowired
    EmploeeDAO emploeeDAO;

    @Override
    @Transactional
    public void addNewEmploee(Emploee emploee) {
        emploeeDAO.addNewEmploee(emploee);
    }

    @Override
    @Transactional
    public void removeEmploee(long id) {
        emploeeDAO.removeEmploee(id);
    }

    @Override
    @Transactional
    public void updateEmploee(Emploee emploee) {
        emploeeDAO.updateEmploee(emploee);
    }

    @Override    
    public List<Emploee> getAllEmploee() {
        return emploeeDAO.getAllEmploee();
    }

    @Override    
    public Emploee getEmploeeById(long id) {
        return emploeeDAO.getEmploeeById(id);
    }

    public EmploeeDAO getEmploeeDAO() {
        return emploeeDAO;
    }

    public void setEmploeeDAO(EmploeeDAO emploeeDAO) {
        this.emploeeDAO = emploeeDAO;
    }    

    @Override
    public Map<Long, String> getAllEmploeeLikeMap() {
        List<Emploee> emploees = emploeeDAO.getAllEmploee();
        Map<Long, String> emploeeMap = new LinkedHashMap<Long, String>();
        for (Emploee emploee: emploees) {
            emploeeMap.put(new Long(emploee.getId()), emploee.getName());
        }
        return emploeeMap;
    }
}
