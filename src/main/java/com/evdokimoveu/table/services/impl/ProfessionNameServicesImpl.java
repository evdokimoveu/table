
package com.evdokimoveu.table.services.impl;

import com.evdokimoveu.table.dao.ProfessionNameDAO;
import com.evdokimoveu.table.entity.ProfessionName;
import com.evdokimoveu.table.services.ProfessionNameServices;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Evdokymov Evgeny
 */


@Service(value="ProfessionNameServicesImpl")
@Transactional
public class ProfessionNameServicesImpl implements ProfessionNameServices, Serializable {

    private static final long serialVersionUID = 1L;
    
    private static final Logger log = Logger.getLogger(ProfessionNameServicesImpl.class);    
    
    @Autowired
    ProfessionNameDAO professionNameDAO;    
    
    @Override
    @Transactional
    public void addNewProfName(ProfessionName professionName) {
        professionNameDAO.addNewProfName(professionName);
    }

    @Override
    @Transactional
    public void removeProfName(long id) {
        professionNameDAO.removeProfName(id);
    }

    @Override
    @Transactional
    public void updateProfName(ProfessionName professionName) {
        professionNameDAO.updateProfName(professionName);
    }

    @Override    
    public List<ProfessionName> getAllProfessoinName() {
        return professionNameDAO.getAllProfessoinName();
    }

    @Override
    public ProfessionName getProfessoinNameById(long id) {
        return professionNameDAO.getProfessionNameById(id);
    }    

    public ProfessionNameDAO getProfessionNameDAO() {
        return professionNameDAO;
    }

    public void setProfessionNameDAO(ProfessionNameDAO professionNameDAO) {
        this.professionNameDAO = professionNameDAO;
    }    

    @Override
    public Map<Long, String> getAllProfessionNameLikeMap() {
        List<ProfessionName> profNames = professionNameDAO.getAllProfessoinName();
        Map<Long, String> emploeeProfName = new LinkedHashMap<Long, String>();
        for (ProfessionName pfName: profNames) {
            emploeeProfName.put(new Long(pfName.getId()), pfName.getName());
        }
        return emploeeProfName;
    }
}
