
package com.evdokimoveu.table.services;

import com.evdokimoveu.table.entity.Emploee;
import com.evdokimoveu.table.entity.Tabel;
import com.evdokimoveu.table.entity.WorkDay;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author Evdokymov Evgeny
 */
public interface WorkDayServices {
    void addNewWorkDay(WorkDay workDay);    
    void updateWorkDay(WorkDay workDay);
    List<WorkDay> getWorkDays(long id);
    WorkDay getWorkDayById(long id);
    void addWorkDayByPackage(long emploeeId, List<WorkDay> tabels, int year, int month);
}
