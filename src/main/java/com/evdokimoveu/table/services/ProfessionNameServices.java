
package com.evdokimoveu.table.services;

import com.evdokimoveu.table.entity.ProfessionName;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Evdokymov Evgeny
 */
public interface ProfessionNameServices {
    public void addNewProfName(ProfessionName professionName);
    public void removeProfName(long id);
    public void updateProfName(ProfessionName professionName);
    public List<ProfessionName> getAllProfessoinName();
    public ProfessionName getProfessoinNameById(long id);
    public Map<Long, String>getAllProfessionNameLikeMap();
}
