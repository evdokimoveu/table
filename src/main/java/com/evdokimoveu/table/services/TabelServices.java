
package com.evdokimoveu.table.services;

import com.evdokimoveu.table.entity.Tabel;

/**
 *
 * @author Evdokymov Evgeny
 */

public interface TabelServices {
    void addNewTabel(Tabel tabel);    
    void updateTabel(Tabel tabel);
    Tabel getTabelById(long id);
    Long getTabelId(long emploeeId, int year, int month);
}
