
package com.evdokimoveu.table.services;

import com.evdokimoveu.table.entity.Emploee;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Evdokymov Evgeny
 */
public interface EmploeeServices {
    public void addNewEmploee(Emploee emploee);
    public void removeEmploee(long id);
    public void updateEmploee(Emploee emploee);
    public List<Emploee> getAllEmploee();
    public Emploee getEmploeeById(long id);
    public Map<Long, String>getAllEmploeeLikeMap();
}
