
package com.evdokimoveu.table.services;

import com.evdokimoveu.table.entity.ProfessionCode;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Evdokymov Evgeny
 */
public interface ProfessionCodeServices {
    public void addNewProfCode(ProfessionCode professionCode);
    public void removeProfCode(long id);
    public void updateProfCode(ProfessionCode professionCode);
    public List<ProfessionCode> getAllProfessoinCode();
    public ProfessionCode getProfessoinCodeById(long id);
    public Map<Long, String>getAllProfessionCodeLikeMap();
}
