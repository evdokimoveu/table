
package com.evdokimoveu.table.services;

import com.evdokimoveu.table.entity.Months;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Evdokymov Evgeny
 */
public interface MonthsServices {
    void updateMonth(Months months);
    void createNewMonthsTable();
    List<Months> getAllMonths();
    Months getMonthById(long id);    
    Map<Long, String> getAllMonthsLikeMap();
}
