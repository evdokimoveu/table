
package com.evdokimoveu.table.services;


import com.evdokimoveu.table.entity.StatDay;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Evdokymov Evgeny
 */
public interface StatDayServices {
    public void addNewStatDay(StatDay statDay);
    public void removeStatDay(long id);
    public void updateStatDay(StatDay statDay);
    public List<StatDay> getAllStatDay();
    public StatDay getStatDayById(long id);
    public Map<Long, String>getAllStatDayLikeMap();
}
