
package com.evdokimoveu.table.util;

import com.evdokimoveu.table.entity.StatDay;
import com.evdokimoveu.table.entity.WorkDay;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import org.apache.log4j.Logger;

/**
 * Class create new List table for tabel.xhtml page 
 * @author Evdokymov Evgeny
 */
public class CreateClearWorkDays {
    
    private static final Logger log = Logger.getLogger(CreateClearWorkDays.class);
    
    private static List<WorkDay> workDays;
    
    /**    
     * @param month
     * @param year
     * @param statDay
     * @return List<WorkDay>
     */
    public static List<WorkDay> getNewWorkDays(int month, int year, StatDay statDay){        
        workDays = new ArrayList<WorkDay>();
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.YEAR, year);
        calendar.get(Calendar.DAY_OF_MONTH);
        
        for(int i = 1; i <= calendar.getActualMaximum(Calendar.DAY_OF_MONTH); i++){
            WorkDay workDay = new WorkDay();            
            calendar.set(Calendar.DAY_OF_MONTH, i);
            if(calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY || calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY){
                workDay.setHours(0);                
            }
            else{
                workDay.setHours(8);                
            }            
            workDay.setDayOfMonth(i);
            workDay.setStatDay(statDay);
            workDays.add(workDay);
        }
        return workDays;
    }
    
}
